require("./index.css");
var $ltMAx$jquery = require("jquery");
require("bootstrap");

function $parcel$interopDefault(a) {
  return a && a.__esModule ? a.default : a;
}



// Генерация модального окна на основе информации вызывающей её событие show карточки
(0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").on("show.bs.modal", function(e) {
    (0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").find("#exampleModalLabel").html((0, ($parcel$interopDefault($ltMAx$jquery)))(e.relatedTarget).find("h2")[0].innerText);
    (0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").find(".modal-body").html((0, ($parcel$interopDefault($ltMAx$jquery)))(e.relatedTarget).find(".card-text").html());
    // Сохраняем карточку, вызывающую модальное окно
    modalCard = (0, ($parcel$interopDefault($ltMAx$jquery)))(e.relatedTarget)[0];
});
// Активация popover'ов (выкл. по дефолту)
(0, ($parcel$interopDefault($ltMAx$jquery)))(function() {
    (0, ($parcel$interopDefault($ltMAx$jquery)))('[data-toggle="popover"]').popover();
});
function $4fa36e821943b400$var$nextCard() {
    const cards = (0, ($parcel$interopDefault($ltMAx$jquery)))(".card");
    let i;
    for(i = 0; i < cards.length; i++){
        if (cards[i] === modalCard) break;
    }
    var nextCardIndex = (i + 1) % cards.length;
    (0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").find("#exampleModalLabel").html((0, ($parcel$interopDefault($ltMAx$jquery)))(".card h2")[nextCardIndex].innerText);
    (0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").find(".modal-body").html((0, ($parcel$interopDefault($ltMAx$jquery)))(".card .card-text")[nextCardIndex].innerHTML);
    modalCard = cards[nextCardIndex];
}
(0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").find(".btn-next").click($4fa36e821943b400$var$nextCard);
function $4fa36e821943b400$var$prevCard() {
    const cards = (0, ($parcel$interopDefault($ltMAx$jquery)))(".card");
    let i;
    for(i = 0; i < cards.length; i++){
        if (cards[i] === modalCard) break;
    }
    var prevCardIndex = i - 1;
    if (prevCardIndex < 0) prevCardIndex = cards.length - 1;
    (0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").find("#exampleModalLabel").html((0, ($parcel$interopDefault($ltMAx$jquery)))(".card h2")[prevCardIndex].innerText);
    (0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").find(".modal-body").html((0, ($parcel$interopDefault($ltMAx$jquery)))(".card .card-text")[prevCardIndex].innerHTML);
    modalCard = cards[prevCardIndex];
}
(0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").find(".btn-prev").click($4fa36e821943b400$var$prevCard);
(0, ($parcel$interopDefault($ltMAx$jquery)))("#exampleModal").keydown((e)=>{
    switch(e.which){
        case 37:
            $4fa36e821943b400$var$prevCard();
            break;
        case 39:
            $4fa36e821943b400$var$nextCard();
            break;
    }
});
(0, ($parcel$interopDefault($ltMAx$jquery)))(".toast").toast({
    "delay": 5000
});
(0, ($parcel$interopDefault($ltMAx$jquery)))("#toastbtn").click(()=>(0, ($parcel$interopDefault($ltMAx$jquery)))(".toast").toast("show"));


//# sourceMappingURL=index.js.map
