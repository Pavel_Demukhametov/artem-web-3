import $ from "jquery";
import header from './views/partials/header.hbs';
import cards from './views/partials/cards.hbs';
import modal from './views/partials/modal.hbs';
import toast from './views/partials/toast.hbs';
import footer from './views/partials/footer.hbs';
import form from './views/partials/form.hbs';
import template4 from './views/partials/modal.hbs';
import template6 from './views/partials/modal_new.hbs';

import objects from '../data/data.json';


import './css/style.css';

function templateRender() {
    $('#header').html(header({companyName: "Мой сайт", buttonText: "Клик"}));
    $('#cards').html(cards({objects}));
    $('#toast').html(toast());
    $('#footer').html(footer());
    $('#modal_1').html(template4());
    $('#new_modal').html(template6());
};

export default templateRender;